
/*
* Algebric-data-types -> are a closed set of possible-subtypes(==enumerated-types) that can be associated with a given-type(enum-class)
*
*Main purpose of each of their instances is to be get assigned to single variable of type enum/sealed-class
*Enumerated-types inherit from their enum-class & can define their own members too
*       but 1.each value is a constant which only exists a single instance  2.each value must have the same members
* The classes which inherit from a sealed-class make-ups the ifs & buts of enum
*       compared to an enumerated class, a sealed class permits inheritance, and its subclasses can contain different states and can have multiple instances.
* */

class Student(var status: StudentStatus)
// a StudentStatus object can have a fixed set of values
enum class StudentStatus {
    NOT_ENROLLED,
    ACTIVE,                                                  // Each enumerated-type inherit from StudentStatus & define their own member doesn't works
    GRADUATED;
    // Used for ACTIVE only, leading to 2 unneeded null states for the property
    var courseId: String? = null                            // doesn't works if included in the body of only 1 of the enumerated types, it needs to be in StudentStatus

}

fun main(args: Array<String>) {
    val student = Student(StudentStatus.NOT_ENROLLED)
    studentMessage(student.status)
}

fun studentMessage(status: StudentStatus): String {
    return when (status) {
        StudentStatus.NOT_ENROLLED -> "Please choose a course."
        StudentStatus.ACTIVE -> "Welcome, student!"
        StudentStatus.GRADUATED -> "Congratulations!"
    }}





















