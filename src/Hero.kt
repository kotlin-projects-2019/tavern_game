val variable = 3 // this variable can be referred in files that sits besides it & the ones who import it
fun main() {
    val adversary: Jhava = Jhava() // Jhava constructor is called; an Instance of Jhava is created
    println(adversary.utterGreeting())

    val friendshipLevel = adversary.determineFreindshipLevel()
    println(friendshipLevel.toLowerCase())  // IntelliJ reports that the method returns a  value of type String!
    // in Java all objects can be null. When you call a Java method like determineFriendshipLevel, the API seems to advertise that the method will return a String
                                            // So Kotlin compiler does not know whether the value of the string being returned from Java is null.
                                            //these are platform type -> Ambiguous types returned to Kotlin from Java code; they may be nullable or non-nullable.
    // authors of Java code can write Kotlin-friendly code that advertises nullity more explicitly using nullability annotations.


}

