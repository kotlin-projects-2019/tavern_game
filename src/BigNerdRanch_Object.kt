import com.hfkotlin.mypackage.Player
import com.hfkotlin.mypackage.extensions.random as randomizer
import java.lang.IllegalStateException
import java.lang.System.exit
import kotlin.system.exitProcess


public fun main() {
    Game.play()
}

object Game { /* named-Singleton */
    private val player = Player("Madrigal")
    private var currentRoom :RoomC = TownSquareC()
    init { p("Welcome, adventurer.") ; player.castFireball() }
    private var worldMap = listOf(
        listOf(currentRoom/*0,0*/, RoomC("Tavern")/*0,1*/, RoomC("Back Room")/*0,2*/),  /*a = 0*/
        listOf(RoomC("Long Corridor")/*1,0*/, RoomC("Generic Room")/*1,1*/) )           /*a = 1*/

    /* home of the game loop */
    fun play() {                                               /* game-loop running as long as app is running */
        var wantToPlay = "yes"
        while (wantToPlay == "yes") {                                                      /* Play NyetHack */
            currentRoom.let { p(it.description(), it.load(), this.playerStatus()) }
            print("> Enter your command: ")

            GameInput(readLine()).run {
                processCommand()
                    .let(::println)
            }
            wantToPlay = "yes"
        }
    }

    private fun playerStatus() :String = "(Aura: ${player.aura}) (Blessed:  ${if (player.isBlessed) "Yes" else "No"})" + player.condition

    private class GameInput(arg :String?) {
        private val inputString = arg?.split(" ") ?: listOf("","")
        val command = inputString[0]
        val argument = inputString.getOrElse(1) {""}

        fun processCommand()
                = when (command.toLowerCase()) {
            "move" -> move(argument)
            "quit","exit" -> quit()
            "map" -> map()
            "fight" -> fight()
            else -> commandNotFound()
        }

        private fun commandNotFound() = "I'm not quite sure what you're trying to do!"
    }
// fun move returns a string based on the result of try-catch expression
    private fun move(directionInput :String)
        = try {
    // player moves into a direction
    val direction = Direction.valueOf(directionInput.toUpperCase()) // fun valueOf is available on all enum classes, returns an enumerated type that matches the string value passed to it otherwise an IllegalArgumentException

    // Is this implies a valid Coordinate ?
    val newCoordinate = direction.updateCoordinate(player.currentCoordinate)

    if(!newCoordinate.isInBounds) throw IllegalStateException("$direction is out of bounds.") // if this one is the last statement in this try block than its an expression

    // If the Coordinate is valid, what room is this ? IF a room doesn't exist for coordinate queried then an ArrayIndexOutOfBoundsException is thrown
    val newRoom = worldMap[newCoordinate.a][newCoordinate.b] // 1st indexing returns a list, a element of the WorldMap; 2nd indexing returns an element from that;
    player.currentCoordinate = newCoordinate
    currentRoom = newRoom

    "OK, you move $direction to the ${newRoom.name}.\n${newRoom.load()}"

    } catch(e :Exception) {
        "Invalid direction: $directionInput."
    }

    private fun quit() {
        println("Thanks for playing NyetHack. See you")
        exit(0)
    }

    private fun map()
        = when (currentRoom.name) {
        "Town Square" -> "x o o\no o"
        "Tavern" -> "o x o\no o"
        "Back Room" -> "o o x\no o"
        "Long Corridor" -> "o o o\nx o"
        "Generic Room" -> "o o o\no x"
        else -> "You are at Unknown Location"
    }

    private fun fight() = currentRoom.monster?.let {
        while (player.healthPoints > 0 && it.healthPoints > 0) {
            this slay it
            Thread.sleep(1000)
        }
        "Combat complete."
    } ?: "There's nothing here to fight."

    private infix fun slay(monster: Monster) {
        println("${monster.name} did ${monster.attack(player)} damage!")
        println("${player.name} did ${player.attack(monster)} damage!")
        if (player.healthPoints <= 0) {
            println(">>>> You have been defeated! Thanks for playing. <<<<")
            exitProcess(0)
        }
        if (monster.healthPoints <= 0) {
            println(">>>> ${monster.name} has been defeated! <<<<")
            currentRoom.monster = null
        }
    }

}

