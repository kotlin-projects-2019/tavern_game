//fun main() {
//    pr("> Enter your command: ") // pr(new Object[]{"> Enter your command: "});
//    p("Last command: ${readLine()}")    // p(new Object[]{"Last command: " + ConsoleKt.readLine()});
//
//    p("> Enter your command: ", "Last command: ${readLine()}") //p(new Object[]{"> Enter your command: ", "Last command: " + ConsoleKt.readLine()});
//
//    print("> Enter your command: ") // String var0 = "> Enter your command: ";
//                                    // System.out.print(var0);
//    println("Last command: ${readLine()}") //  var0 = "Last command: " + ConsoleKt.readLine();
//                                             // System.out.println(var0);
//
//    pr(*arrayOf<Any>("> Enter your command: "))
//    p(*arrayOf<Any>("Last command: " + readLine()!!))
//    p(*arrayOf<Any>("> Enter your command: ", "Last command: " + readLine()!!))
//    var var0 = "> Enter your command: " ; print(var0)
//    var0 = "Last command: " + readLine()!! ; println(var0)
//
//}
//
class GameInput(val arg :String? = readLine()) {
    fun f() = p(arg)
}

fun main() {
    GameInput().f()
}