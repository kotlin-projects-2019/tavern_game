import  java.util.Random

interface Fightable {   // interface declaration to define what a creature needs to engage in combat
    var healthPoints :Int
    val diceCount :Int
    val diceSides :Int
    val damageRoll :Int
    get() = (0 until diceCount).map {
        Random().nextInt(diceSides + 1)
    }.sum()

    fun attack(opponent :Fightable) :Int
}

abstract class Monster(val name :String, val description :String, override var healthPoints :Int) :Fightable {
    override fun attack(opponent: Fightable): Int {
        val damageDealt = damageRoll
        opponent.healthPoints -= damageDealt
        return damageDealt
    }
}
/*
even though `Abstract-class-Monster` implements `Fightable-interface`
It does not have to include all the requirements of the Fightable interface,
because it is an abstract class and will never be instantiated.
Subclasses of the `Abstract-class-Monster`, however,
must implement all requirements of `Fightable-interface` , either through
        - inheritance from Monster or
        - on their own.
*/
class Goblin(name: String = "Goblin",
             description: String = "A nasty-looking goblin",
             healthPoints: Int = 30) : Monster(name, description, healthPoints) {
    override val diceCount = 2
    override val diceSides = 8
}