//abstract class Pet(var name :String)    // no need to initialize here , so no need to mark abstract too // & if want to make a val-property as function add getter to it in the body
//class Cat(name :String) :Pet(name)
//class Dog(name :String) :Pet(name)
//class Fish(name :String) :Pet(name)
//class Contest<T :Pet>(var vet :Vet<in T>)   // as we want each contest to be limited to a specific type of pet, we’ll define the Contest class using generics. // Cat-contest ; Fish-contest ; Dog-contest -> Same contest - code for them
//// So the above code means that you can create Contest objects that deal with Cats, Fish or Pets, but not Bicycles or Begonias.
//{
//    val contestantScores :MutableMap<T, Int> = mutableMapOf() // keep track of which-contestant(Key i.e. Pet-objects) receives which-score(Value i.e. Int)
//    fun addScore(contestant :T, score :Int = 0)
//    {
//        if (score > 0) this.contestantScores[contestant] = score
//    }
//    fun getWinners() :MutableSet<T>
//    {
//        val winners: MutableSet<T> = mutableSetOf() // a mutable set as it allows later addition of values but also in the main function
//        for ((c, s) in this.contestantScores) if (s == this.contestantScores.values.max()) winners.add(c)
//        return winners
//    }
//    fun getWinners2() :Set<T>
//    {
//        val highScore = contestantScores.values.max()
//        val winners = contestantScores.filter{ it.value == highScore } // filter function used with a map , so it will return a map of the same type having value a/q to lambda
//                            .keys  // returns a set of pet-objects
//        winners.forEach { println("Winner: ${it.name}")}
//        return winners
//
//    }
//
//}
//
//class Vet<T: Pet>   // As vets can specialize in treating different types of pet, we’ll create a Vet class with a generic type T, and specify that it has a treat function that accepts an argument of this type. We’ll also say that T must be a type of Pet so that you can’t create a Vet that treats, say, Planet or Broccoli objects.
//{
//
//    fun treat(to :T) = println("Treat ${to.name}")
//}
//fun main() {
//    val catContest = Contest<Cat>(Vet<Pet>()) // creates a cat- Contest that has petVet but should have catVet ;) how??
//   var a =  catContest.apply {
//        addScore(Cat("Fuzz"), 50)
//        addScore(Cat("Katsu"), 45)
//        val set = getWinners()
//        set.add(Cat("puchi"))
//        println(" Winner is ${getWinners().first().name} ; Print set ${set}")
//        println(getWinners().first().toString())
//        println(getWinners().first())
//   }
//
//    val petContest: Contest<Pet> = Contest(Vet<Pet>())   // if variable is given explicit type then the compiler can infer that any new-Contest is used with Pets
//    petContest.apply{
//        addScore(Fish("Wanda"), 3000)
//        addScore(Dog("Shiro"), 3000)
//        println(" Winner is ${getWinners().first().name} ; Print set ${getWinners()}")
//    }
//
//
//    val v : Retailer<Pet> = CatRetailer() //  won't work if out is not prefixed in the interface / This behavior appears to violate the whole point of polymorphism.
//    /*
//    val v2 : Retailer<Pet> = PetRetailer() //  works only if out not prefixed
//    we can adjust the generic type in the Retailer interface to control which types of objects a Retailer<Pet> variable can accept.
//    If you want to be able to use a generic subtype object in a place of a generic supertype, you can do so by prefixing the generic type with out.
//    When we prefix a generic type with out, we say that the generic type is
//covariant. In other words, it means that a subtype can be used in place of a
//supertype.
//
//   */
//    println()
//println(a)
//    println(catContest)
//}
//
//
//interface Retailer<out T> // use generics to ensure that each type of Retailer can only sell a specific type of pet, so that you can’t buy a Cat from a FishRetailer. Each implementation of the Retailer interface must specify the type of object it deals with by replacing the “T” defined in the interface with the real type.
//{
//    val area :Int  // no need to use abstract keyword ; so better than abstract class
//    fun sell() :T
//}
////class PetRetailer :Retailer<Pet>
////{
////    override val area: Int = 500
////    override fun sell() :Cat
////    {
////        println("Pet(cat) sold")
////        return Cat("")
////    }
////}
//
//class CatRetailer :Retailer<Cat>  // Cat retailer replaces Retailer's generic type
//{
//    override val area: Int = 50
//    override fun sell() :Cat
//    {
//        println("Cat sold")
//        return Cat("")
//    }
//}
//
//
//
//
