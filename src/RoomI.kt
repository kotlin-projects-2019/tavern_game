interface RoomI { val name :String
    /*protected*/ open val dangerLevel     // property can't be initialized in a interface
        get() = 5       // so the way to add default value to a val
    public open fun description() = "Room: $name\nDanger Level: $dangerLevel"
    public abstract fun load() :String
//    override  fun hashCode() {
//        // An Interface Can't Implement A Method of Any
//    }
}

open class TownSquareI :RoomI { override val name = "Town Square"
    override val dangerLevel = super.dangerLevel - 3
    private val bellSound = "GWONG"
    override fun load() = "The villagers rally & cheer as you enter!\n${ringBell()}"
    fun ringBell() = "The Bell Tower announces your arrival. $bellSound"
}

fun main() {
    val currentRoom :RoomI = TownSquareI()
    currentRoom.run {
        p(description(), load()/*, ringBell()*/)
    }
    (currentRoom as TownSquareI).ringBell().let(::println)
    val abandonedTownSquareI = object : TownSquareI() {
        override fun load() = "You anticipate applause, but no one is here..."
    }
}
/*
interface allows to specify common properties & behaviour
that are supported by a subset of classes
Compared to abstract-class they can't define constructors or act as a superclass

To use an interface, we say that you "implement" it on a class
There are two parts to this:
    First, you declare that the class implements the interface.
    Then, you must ensure that the class provides implementations for all of the properties and functions specified in the interface.

 Use an interface: When you need a category of behavior or properties
 that objects have in common
 that does not fit using inheritance,
 Use an abstract-class: When inheritance makes sense – but you do not want
a concrete parent class – then an abstract class may make sense

These tools let you create relationships that focus on what a class can do rather than how it does it
 */

