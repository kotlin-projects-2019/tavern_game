import java.io.File
import java.time.LocalDate
import java.time.chrono.ChronoLocalDate
/* Kotlin’s built-in types come with a range of available operations and that some types tailor those operations based on the data they represent.
When you want to use built-in operators with your custom types, you have to override the operators’ functions to tell the compiler how to implement them for your type.
This is known as operator overloading.*/
fun operatorFunctions() {   //single parameter member & extension functions can be "upgraded" to operators, allowing their calls with the corresponding operator symbol.
    operator fun Int.times(str :String) = str.repeat(this)
    val a = 2 * "Bye"
    // extension fun times with a single parameter turned into operator fun

    operator fun String.get(range :IntRange) = /*this.*/substring(range)        // this is optional
    val str = "Always forgive your enemies; nothing annoys them so much"
    val b = str[0..14]
}

fun infixFunctions()  {
// Member functions and extensions with a single parameter can be turned into infix functions.
    infix fun Int.times(str :String) = str.repeat(this)
    val a = 2 times "Bye"
        // OR
    val b = 2.times("Bye")

    val pair = "Ferrari" to "Audi"
    println(pair)   // (Ferrari, Audi)

    infix fun String.onto(other :String) = Pair(this, other) // Here's your own implementation of `to` creatively called `onto`. The containing class becomes the first parameter.
    val myPair = "McLaren" onto "Tesla"

    val sophia = Person("Sophia")
    val claudia = Person("Claudia")
    sophia likes claudia
    sophia[claudia]
    val (n, l, h) = sophia
}

class Person(val name :String)      // declare a class with one property ; declare a constructor with one parameter ~~ class Person( name :String ) { val name = name }
{
    val legs = 2
    val hands = 2
    val likedPeople = mutableListOf<Person>()
    infix fun likes(other :Person)      // The containing class becomes the first parameter.

    {
        this.likedPeople.add(other)     // 1st parameter(this) comes naturally & is optional to write
    }
// member function like with a single parameter turned into infix
    operator fun get(other :Person)
    {
         likedPeople.add(other)
    }
    // operator fun get as a member fun with a single parameter

    operator fun component1() = name
    operator fun component2() = legs
    operator fun component3() = hands
}
fun varargParameters()
{
    fun printAll(vararg messages :String, prefix :String = "") {
        for (r in messages) println(prefix + r)     // At runtime, a vararg is just an array.
    }
    printAll("Hello", "Hallo", "Salut", "Hola")
    printAll("Hello", "Hallo", "Salut", "Hola", prefix = "Greeting")
    fun log (vararg entries :String)
    {
        printAll(*entries)  // To pass a vararg into a vararg parameter, use the special spread operator * that lets you pass in *entries (a vararg of String) instead of entries (an Array<String>).
    }
}
fun variableWithoutInitialization()
{
    // You're free to choose when you initialize a variable, however, it must be initialized before the first read.
    var e: Int  // 1
    if (readLine() == "yes") {
        e = 1   // 2
    } else {
        e = 2   // 2
    }
    println(e)  // 2

    val a = mutableListOf("hero", "hira")
    val b = joinOptions(a)
}
fun joinOptions(options: Collection<String>) = options.joinToString(prefix = "[", postfix = "]")    // Default and named arguments help to minimize the number of overloads and improve the readability of the function invocation. The library function joinToString is declared with default values for parameters: It can be called on a collection of Strings. Specifying only two arguments make the function joinOptions() return the list in a String of JSON format (e.g., "[a, b, c]")

fun foo (name :String, number :Int = 42, toUpperCase :Boolean = false)  = (if (toUpperCase) name.toUpperCase() else name) + number

fun generics()
{ // Generic classes and functions increase code reusability by encapsulating common logic that is independent of a particular generic type, like the logic inside a List<T> is independent of what T is.
//generic class ->
    class MutableStack<T> (vararg items :T)
    {// Defines a generic class MutableStack<T> where T is called the generic type parameter. At use-site, it is assigned to a specific type such as Int by declaring a MutableStack<Int>.
    private val elements = items.toMutableList()
    fun push(element :T) = elements.add(element)    // can use T as a parameter type
    fun peek() = elements.last()    // can use T as a return type
    fun pop() = elements.removeAt(elements.size - 1)
    fun isEmpty() = elements.isEmpty()
    val size
    get() = elements.size
    override fun toString() = "MutableStack(${elements.joinToString()})"
    }
    // generify functions if their logic is independent of a specific type. For example, a helper/utility function ro create a MutableStack
    fun <T> mutableStackOf(vararg items :T) = MutableStack(*items)    // class MutableStack
    val stackC = MutableStack(0.62,3.14,2.7)
    val stackF = mutableStackOf(0.62,3.14,2.7)
}

fun higher_order_functions() {
    // Taking Functions as Parameters
    fun calculate(x: Int = 0, y: Int = 0, operation: (Int, Int) -> Int): Int {
        return operation(x, y)
    }

    fun sum(x: Int, y: Int) = x + y
    val sumResult = calculate(operation = ::sum)  // function pointer - :: is the notation that references a function by name in Kotlin.
    val mulResult = calculate { a, b -> a * b }  // Invokes the higher-order function passing in a lambda as a function argument.

    // Returning Functions
    fun square(x: Int) = x * x

    fun operation(): (Int) -> Int {                                     // 1
        return ::square
    }

    fun main() {
        val func = operation()                                          // 3
        println(func(2))                                                // 4
    }

    // All examples create a function object(lambda)
    val upperCase1: (String) -> String = { it.toUpperCase() }
    val upperCase6: (String) -> String =
        String::toUpperCase // If your lambda consists of a single function call, you may use function pointers (::) .
}

fun list()  // list -> an ordered collection of items
{
    val systemUsers :MutableList<Int> = mutableListOf(1,2,3)
    val sudoers :List<Int> = systemUsers as List<Int>   // read-only_view of mutable list by casting it to list
    fun addSudoer(newUser :Int)
    {
        systemUsers.add(newUser)    // Updates the MutableList. All related read-only views are updated as well since they point to the same object.
    }
    addSudoer(4)
    systemUsers[1]  ; systemUsers.get(1)// index-access-operator calls a function get() to retreive an element from a list
    val patronList = mutableListOf("Eli", "Mordoc", "Sophie")
    println(patronList)
    patronList.add("Alex")
    patronList.add(0, "Alex")
    println(patronList) // [Eli, Mordoc, Sophie][Alex, Eli, Mordoc, Sophie, Alex]

    patronList[0] = "Alexis"
    println(patronList) // [Alexis, Eli, Mordoc, Sophie, Alex]
//Mutator-Functions:-
    // getOrElse(index) {}
    // getOrNull(index)
    // arrayName[index]= eToReplace
    // add(index, eToInsert)

    // += e/listOf(e)
    // -= e/listOf(e)
    // add(eToAddInTheLast)
    // addAll(listOf(e)
    // contains(e)
    // containsAll(listOf(e)
    // remove(e)
    // removeAll(listOf(e)
    // clear()
    // removeIf { it -> true/false-condition }

    val menuList = File("data/tavern-menu-data.txt").readText().split("\n")
// Reading a File into a List:-
    // read the text from file into a String
    // then for a list call split on it
    p(menuList)
    menuList.forEachIndexed { i, e ->
        p("$i : $e")
    }

    val (n, b, c, d, e ) = menuList
    // A list also offers the ability to destructure up to the first five elements it contains.

    val a = listOf(mutableListOf(1,2,3), mutableListOf(1,2,3))  // list contains mutable-lists & their contents can be modified
    a[0].add(4)

    val x = listOf(1,2)
    //  x += 3 // can't happen as reassignment needed to val x  // += for a val mutableList works like add()
    var y = listOf(1,2)
    y += 4 // can happen as reassignment can done to var y   // as += creates a new list under the hood & assigns this new list to the var
    val g = mutableListOf(1)
    g +=  3
    var h = mutableListOf(1)
    h.plusAssign(4)
    /* += or -=                                                                 // main function to do is to create a new list & assign its reference
    for var - collection -> assigns new modified collection
    for val - collection -> nothing; exception: can't assign to val
    for val - mutableCollection -> adds/removes element                          // but in case of mutableCollections it adds element to the existing list
    for var - mutableCollection -> results in ambiguity, need to use plusAssign
     */


    // when casted items in a val can only be replaced as it doesn't requires reassignment & to var resizing can also be done
    // val x is immune to += & -= & any other that does resizing
    (x as MutableList)[0] = 5   // but replacements can be done     // but if the list at the time of creation had only  1 element than this cannot happen
    (y as MutableList).addAll(listOf(4,64,454))

    x.addAll(listOf(1))         // will not happen due to val
    y.addAll(listOf(4,64,454)) // will happen due to var




}
//So what is a programmer who wants unique elements and high-performance, index-based access to do?
//Use both: Create a Set to eliminate duplicates and convert it a to a List when index-based access or mutator functions are needed
// you create a mutable set of unique patron names by feeding the elements from a list into it, one by one
fun set()   // set -> unordered collection , doesn't support duplicates
{
    val openIssues = mutableSetOf("uniqueDescr1", "uniqueDescr2", "uniqueDescr3")
    fun addIssue(uniqueDescr :String) :Boolean = openIssues.add(uniqueDescr)    // Returns a boolean value showing if the element was actually added.
    fun getStatusLog(isAdded :Boolean) = if (isAdded) "registered correctly" else "marked as duplicate & rejected"

    val aNewIssue = "uniqueDescr4"
    val anIssueAlreadyIn = "uniqueDescr2"
    println("Issue $aNewIssue ${getStatusLog(addIssue(aNewIssue))}")
    println("Issue $anIssueAlreadyIn ${getStatusLog(addIssue(anIssueAlreadyIn))}")
}

const val POINTS_X_PASS :Int = 15
fun map()   // map -> collection of key-value pairs not items
{
   val EZPassAccounts = mutableMapOf(1 to 100, 2 to 100, 3 to 100)
   val EZPassReport :Map<Int, Int> = EZPassAccounts // creates a read-only view of the map
   fun updatePointsCredit(accountId :Int)
   {
       if(EZPassAccounts.containsKey(accountId))
       {
           println("Updating $accountId...")
           EZPassAccounts[accountId] = EZPassAccounts.getValue(accountId) + POINTS_X_PASS
       } else {
           println("Error: Trying to update a non-existing account (id: $accountId")
       }
   }
   fun accountsReport()
   {
       println("EZ-Pass report:")
       EZPassReport.forEach { (k, v) ->   // it.key, it.value       // the pair passed to as argument to the block body got destructured
           println("ID $k: credit $v")
       }
   }
   accountsReport()
   updatePointsCredit(1)
   updatePointsCredit(1)
   updatePointsCredit(5)
   accountsReport()
}

fun filter() // return a list of those references from the given list for which the predicate(=criteria in lambda) is true
{
    val numbers = listOf(1, -2, 3, -4, 5, -6)
    val positives = numbers.filter { true } // [1, -2, 3, -4, 5, -6]
    val negatives = numbers.filter { it < 0 }
}

// A Transform
fun mapExtensionFunction() //  returns a list of return values of each expression in lambda         //  the items/objects/references in the collection are available as it, means:-
{                                                                                                   //  for (element in this) ~~ this -> the collection/string | element ~~ constituents
    val numberss = listOf(1, -2, 3, -4, 5, -6)
    val doubled = numberss.map { it * 2}    // [2, -4, 6, -8, 10, -12]
    "string".map { it } // [s, t, r, i, n, g]

    val numbers = listOf(7, 4, 8, 4, 3, 22, 18, 11)
    val primes = numbers.filter {    // gives 7
        (2 until it).map { d ->          // divides 7 by 2,3,4,5,6
            it % d                           // a list of remainders for 7 are generated
        }.none { it == 0 }                // for 7 to be prime none of the numbers in remainders-list should be equal to 0/for 7 to be prime the remainders-list should not contain 0
    }
    print(primes)
}
// A Transform
fun flatMap()   // returns a single, “flattened” collection containing all of the elements of the input collections.
{
    val numbers = listOf(1, -2, 3, -4, 5, -6)
    val a = numbers.flatMap {// the flatMap needs list inside of its lambda for each iteration, either you give list as `it` directly or make a list inside
        listOf(it + 1, it + 1, it + 1 )} // [2, 2, 2, -1, -1, -1, 4, 4, 4, -3, -3, -3, 6, 6, 6, -5, -5, -5]
    val b = numbers.flatMap { listOf(it + 1, it + 2, it + 3 )} //  [2, 3, 4, -1, 0, 1, 4, 5, 6, -3, -2, -1, 6, 7, 8, -5, -4, -3]
    //  the result not a list of lists; it's a list of integers with nine elements. | 1stmutableList += 2ndList += 3rdList

    val itemsOfManyColors = listOf(listOf("red apple", "green apple", "blue apple"), listOf("red fish", "blue fish"), listOf("yellow banana", "teal banana"))
    val redItems = itemsOfManyColors.flatMap {
        it.filter {
            "red" in it } }
    print(redItems)
}

fun any() // returns true if the collection contains at least one element that matches the given criteria in lambda
{
    val numbers = listOf(1, -2, 3, -4, 5, -6)
    val anyNve = numbers.any { it < 0 }
    val anyGt5 = numbers.any { it > 5 }
}

fun all() // returns true if all elements in collection match the given predicate(=criteria in lambda)
{
    val numbers = listOf(1, -2, 3, -4, 5, -6)
    val allEven = numbers.all { it % 2 == 0 }
    val allLess6 = numbers.all { it < 6 }
}

fun none() // returns true if no element match the given predicate
{
    val numbers = listOf(1, -2, 3, -4, 5, -6)
    val allEven = numbers.none { it % 2 == 1 }  // logically opposite of all
    val allLess6 = numbers.none { it > 6 }
}

fun find() // gets the reference? of the first/last matching element
{
    val sentence = "Let's find something in collection somehow"
    val words = sentence.split(" ")
    val first = words.find { it.startsWith("some", true)}
    val last = words.findLast { it.startsWith("some", true)}
    val nothing = words.find { it.contains("nothing")} // "nothing" in it  ; both possible only for strings (may-be)
        // OR
    words.find { "nothing" in it }

    val json = words.joinToString(prefix = "[", postfix = "]", separator = "\n")
    (1..3).toList()
    words.count()

}

// first, last, firstOrNull, lastOrNull, count, min , max

fun use_of_count()
{
    var a = "Mississippi".count()   //when called on a string     // returns "Mississippi".size()
    val sentence = "Let's find something in collection somehow"
    val words = sentence.split(" ")
    var b = words.count()       // returns words.size()

    var c = words.count { "nothing" in it }
    var d = "Mississippi".count { it == 's' }     // in the body of count() ~~ for (element in "Mississippi") ~~ `it` is the element ; which in this case will be a Char
}

fun associateByGroupBy()   // associateBy -> generates a map of pairs // groupBy -> generates a map of common-key to object-list
{
    data class Person(val name: String, val city: String, val phone: String)

    val people = listOf(
        Person("John", "Boston", "+1-888-123456"),
        Person("John", "Boston", "+1-888-123456"),
        Person("Sarah", "Munich", "+49-777-789123"),
        Person("Sarah", "Munich", "+49-777-789123"),
        Person("Svyatoslav", "Saint-Petersburg", "+7-999-456789"),
        Person("Svyatoslav", "Saint-Petersburg", "+7-999-456789"),
        Person("Vasilisa", "Saint-Petersburg", "+7-999-123456"),
        Person("Vasilisa", "Saint-Petersburg", "+7-999-123456"))

    // phone of 1st object in List<object> taken as key & object is then added to it :-
    val phoneBook  = people.associateBy { it.phone }    // this will return a Map<phone, person>
    // each-phone to each-Person | keySelector - lambda | the value of each mapElement is PersonObject as optional valueSelector is not provided
    val phoneBook2  = people.groupBy { it.phone }   // this will return a Map<phone, List<person>>
    // eachCommon-phone to ListOf-Persons


    val cityBook  = people.associateBy(Person::phone, Person::city)     // 4 unique phone, so 4 unique key formed, and values updated to it
    //  each-phone to each-city | valueSelector - Person::city
    // {
    //      +1-888-123456=Boston,
    //      +49-777-789123=Munich,
    //      +7-999-456789=Saint-Petersburg,
    //      +7-999-123456=Saint-Petersburg
    // }
    val cityBook2 = people.groupBy(Person::phone, Person::city)     // 4 unique phone, so 4 unique key formed, and values added to its corresponding list
    // eachCommon-phone to ListOf-cities  | valueSelector - Person::city
    // {
    //      +1-888-123456=[Boston, Boston],
    //      +49-777-789123=[Munich, Munich],
    //      +7-999-456789=[Saint-Petersburg, Saint-Petersburg],
    //      +7-999-123456=[Saint-Petersburg, Saint-Petersburg]
    //  }

    val peopleCities  = people.groupBy{ it.city }    // eachCommon-city to ListOf-Persons
    // how groupBy works - on each iteration it takes a key from lambda , & updates the value(=adds an Object in the corresponding list) of that key
    // how associateBy works - on each iteration it takes a key from lambda , & updates the value(=an Object) of that key
    var id = 0
    val peopleId = people.associate { id++ to it }  // lambda requires the pair to be formed | in associateBy & groupBy lambda requires only key as by default the value is the object in the List<object> on which they are called
    var i = 0
    var a = people.associate { i++ to it }
    // more convinient if associateBy is used as the object of Collection<Object> on which associateBy is called on is used as value by default

    var b  = people.associateBy { i++ }
    // {
    //      0=Person(name=John, city=Boston, phone=+1-888-123456),
    //      1=Person(name=John, city=Boston, phone=+1-888-123456),
    //      2=Person(name=Sarah, city=Munich, phone=+49-777-789123),
    //      3=Person(name=Sarah, city=Munich, phone=+49-777-789123),
    //      4=Person(name=Svyatoslav, city=Saint-Petersburg, phone=+7-999-456789),
    //      5=Person(name=Svyatoslav, city=Saint-Petersburg, phone=+7-999-456789),
    //      6=Person(name=Vasilisa, city=Saint-Petersburg, phone=+7-999-123456),
    //      7=Person(name=Vasilisa, city=Saint-Petersburg, phone=+7-999-123456)
    //  }
    var c = people.groupBy { i++ }
    // {
    //       8=[Person(name=John, city=Boston, phone=+1-888-123456)],
    //       9=[Person(name=John, city=Boston, phone=+1-888-123456)],
    //       10=[Person(name=Sarah, city=Munich, phone=+49-777-789123)],
    //       11=[Person(name=Sarah, city=Munich, phone=+49-777-789123)],
    //       12=[Person(name=Svyatoslav, city=Saint-Petersburg, phone=+7-999-456789)],
    //       13=[Person(name=Svyatoslav, city=Saint-Petersburg, phone=+7-999-456789)],
    //       14=[Person(name=Vasilisa, city=Saint-Petersburg, phone=+7-999-123456)],
    //       15=[Person(name=Vasilisa, city=Saint-Petersburg, phone=+7-999-123456)]
    //  }
    var d = people.associateBy { i++ to it.city }           // the key ( here its a pair )
    // {
    //      (16, Boston)=Person(name=John, city=Boston, phone=+1-888-123456),
    //      (17, Boston)=Person(name=John, city=Boston, phone=+1-888-123456),
    //      (18, Munich)=Person(name=Sarah, city=Munich, phone=+49-777-789123),
    //      (19, Munich)=Person(name=Sarah, city=Munich, phone=+49-777-789123),
    //      (20, Saint-Petersburg)=Person(name=Svyatoslav, city=Saint-Petersburg, phone=+7-999-456789),
    //      (21, Saint-Petersburg)=Person(name=Svyatoslav, city=Saint-Petersburg, phone=+7-999-456789),
    //      (22, Saint-Petersburg)=Person(name=Vasilisa, city=Saint-Petersburg, phone=+7-999-123456),
    //      (23, Saint-Petersburg)=Person(name=Vasilisa, city=Saint-Petersburg, phone=+7-999-123456)
    // }
    people.associateBy { i to it.city }     // 3 unique cities only, so 3 unique keys formed and objects from List<object> were added
    // {
    //      (24, Boston)=Person(name=John, city=Boston, phone=+1-888-123456),
    //      (24, Munich)=Person(name=Sarah, city=Munich, phone=+49-777-789123),
    //      (24, Saint-Petersburg)=Person(name=Vasilisa, city=Saint-Petersburg, phone=+7-999-123456)
    //  }


}

fun associateByUpdatesTheValueOfTheKeys() {
    data class Person(val name: String, val city: String, val phone: String)

    val people = listOf(
        Person("Joh", "Boston", "+1-888-123456"),
        Person("John", "Boston", "+1-888-123456"),
        Person("Sara", "Munich", "+49-777-789123"),
        Person("Sarah", "Munich", "+49-777-789123"),
        Person("Svyatos", "Saint-Petersburg", "+7-999-456789"),
        Person("Svyatoslav", "Saint-Petersburg", "+7-999-456789"),
        Person("Vasil", "Saint-Petersburg", "+7-999-123456"),
        Person("Vasilisa", "Saint-Petersburg", "+7-999-123456")
    )

     val d =  people.associateBy { it.phone }
   // {+1-888-123456=Person(name=John, city=Boston, phone=+1-888-123456), +49-777-789123=Person(name=Sarah, city=Munich, phone=+49-777-789123), +7-999-456789=Person(name=Svyatoslav, city=Saint-Petersburg, phone=+7-999-456789), +7-999-123456=Person(name=Vasilisa, city=Saint-Petersburg, phone=+7-999-123456)}


}

fun partition() // splits the collection in a pair of lists
{
    val numbers = listOf(1, -2, 3, -4, 5, -6)
    val (even, Odd) = numbers.partition { it % 2 == 0 } // ([-2, -4, -6], [1, 3, 5])    // pair destructuring is applied to get pair members
}

fun sorted_by() {
    val shuffled = listOf(5, 4, 2, 1, 3)
    val natural = shuffled.sorted()
    val inverted = shuffled.sortedBy { -it } //  natural sort order of the values returned by lambda
}

fun mapElementAccess()
{
    val map = mapOf("key" to 43)
    val v1  = map["key"] //  // [] operator returns the value corresponding to the given key, or null if there is no such key in the map.
    val v2 = map.getValue("key") // getValue function returns an existing value corresponding to the given key or throws an exception if the key wasn't found. so dangerous to use
    try {
        map.getValue("anotherKey")
    } catch (e: Exception) {
        println("Message: $e")
    }
    val safeMapToGetValue = map.withDefault { key -> key.length  }  // lambda provides the default value in case the key doesn't exist
    val v3 = safeMapToGetValue.getValue("none") // 4
}

fun zip()
{
    val A = listOf("a", "b", "c")
    val B = listOf(1, 2, 3, 4)
    val resultPairs = A zip B            // [(a, 1), (b, 2), (c, 3)]
    resultPairs.toMap()     // {a=1, b=2, c=3} |  whenever you have a list of Pairs, to return a map that can be indexed into using a key – here, an employee name.
    val resultReduce = A.zip(B) { a, b -> "$a$b" }  // [a1, b2, c3]

    val employees = listOf("Denny", "Claudette", "Peter")
    val shirtSize = listOf("large", "x-large", "medium")
    val employeeShirtSizes = employees.zip(shirtSize).toMap()
    println(employeeShirtSizes["Denny"])


}

fun getOrElse()
{
    val list = listOf(0, 10, 20)
    println(list.getOrElse(1) { 42 })   // 0 // element at index 1
    println(list.getOrElse(10) { 42 })  // 42 // index 10 is out of bounds

    val map = mutableMapOf<String, Int?>()
    println(map.getOrElse("x") { 1 })       // 1 // key "x" is not in map

    map["x"] = 3
    println(map.getOrElse("x") { 1 })   // 3 // value of key "x"

    map["x"] = null
    println(map.getOrElse("x") { 1 })   // 1 // value for key "x" is not defined

}

fun destructuringDeclarations()
{
    fun findMinMax(list: List<Int>): Pair<Int, Int> {
        // do the math
        return Pair(50, 100)
    }

        val (x, y, z) = arrayOf(5, 10, 15)    // Destructures an Array.

        val map = mapOf("Alice" to 21, "Bob" to 25)
        for ((name, age) in map) {      // Maps can be destructured
            println("$name is $age years old")
        }



        val (min, max) = 50 to 100
        val (minn, maxx) = findMinMax(listOf(100, 90, 50, 98, 76, 83))    //Built-in Pair and Triple types support destructuring too, even as return values from functions.

    data class User(val username: String, val email: String)    // 1

    fun getUser() = User("Mary", "mary@somewhere.com")


        val user = getUser()
        val (username, email) = user
        println(username == user.component1())    // Data class automatically defines the component1() and component2() methods that will be called during destructuring.

        val (_, emailAddress) = getUser()    //Use underscore if you don't need one of the values, avoiding the compiler hint indicating an unused variable.

    class Pair<KeyType, ValueType>(val first: KeyType, val second: ValueType) {  // Defines a custom Pair class with component1() and component2() methods.
        operator fun component1(): KeyType {
            return first
        }

        operator fun component2(): ValueType {
            return second
        }
    }
        val (num, name) = Pair(1, "one")             // Destructures an instance of this class the same way as for built-in Pair.

        println("num = $num, name = $name")

}

/**
 * ~~ is operator ~~
to check the type of the underlying object that’s referenced by the variable.
>	Casting means
that the compiler treats a variable as though its type is different to the one that it’s declared as
>	Use as to perform an explicit cast
If you want to access the behavior of an underlying object but the compiler can’t perform a smart cast, you can explicitly cast the object into the appropriate type.
var x = 3
var y :String = x as String // Warning:(3, 23) Kotlin: This cast can never succeed but the code compiled
println(y)

~~ as operator ~~
val r :Roamable = Wolf()
if ( r is wolf )
{
val wolf = r as Wolf
wolf.eat()
}
But if r is assigned a value of some other type in between the type-check and
the cast, the system will throw a ClassCastException.
The safe alternative is to perform a safe cast using the as? operator using
code like this:
val wolf = r as? Wolf // This casts r as a Wolf if r holds an object of that type, and returns null if it doesn’t.
This saves you from getting a ClassCastException if your
assumptions about the variable’s type are incorrect.
as? lets you perform a safe explicit cast. If the cast fails, it returns null.
 */
fun smartCast()
{// The Kotlin compiler is smart enough to perform type casts automatically in most cases, including:
// 1. Casts from nullable types to their non-nullable counterparts. 2. Casts from a supertype to a subtype.
// This way, you can automatically use variables as desired in most cases without doing obvious casts manually.
    val date :ChronoLocalDate? =  LocalDate.now()

    if (date != null)
    {
        val d = date as ChronoLocalDate     // direct date can be used in place of d    // date? casted to date ~ nullable to non-nullable
        println(d.isLeapYear)
    }

    if (date != null && date.isLeapYear) {          // Smart-cast inside a condition (this is possible because, like Java, Kotlin uses short-circuiting)
        println("It's a leap year!")
    }

    if (date == null || !date.isLeapYear) {         // Smart-cast inside a condition (also enabled by short-circuiting).
        println("There's no Feb 29 this year...")
    }

    if (date is LocalDate) {
        val month = date.monthValue     // Smart-cast to the subtype LocalDate.
        println(month)
    }
    if (date is LocalDate) {
        val m = date as LocalDate
        val month = m.monthValue
        println(month)
    }



}

fun fold()
{
    val foldedValue = listOf(1, 2, 3, 4).fold(0) { accumulator, number ->
        println("Accumulated value: $accumulator")
        accumulator + (number * 3)
    }                                      // 1st value of accumulator is 0, after that the result of lambda
    println("Final value: $foldedValue")
}














