/*
A class(known as enum) defining a collection of constants called enumerated types; all
instances of the class are of one of the defined types. Compared to a
sealed class, an enumerated class prohibits inheritance, and its subclasses
cannot contain different states or have multiple instances.
*/
enum class BandMember ( val instrument :String)
// a property named instrument
// this enum class has 3 values(=constants)/instances/enumerated-types
// weird---Each value is an instance of this enum-class so can have a constructor to initialize each enum value and thus each value has the property defined in the constructor     // MAY BE JUST LIKE AN INT, int is itself a value but a class too that has its own member functions HENCE THE CONCEPT OF initialize each value in the class with an appropriate value.
{
    JERRY("lead guitar")         // JERRY is the BandMember-instance
    {
        override fun sings() = "plain"
    }
    ,BOBBY("rhythm guitar")      // () is the BandMember-constructor
    ,PHIL("bass")                // argument in () is passed to instrument property of the respective instance

    ; open fun sings() = "occasionally"
}
class Duck(name :String = "", gender :Char = 'n')
{
    object DuckFactory  //Class-object (it’s shared by all instances of that class.)   // object declaration. It isn't an expression, and can't be used in a variable assignment. You should use it to directly access its members:

    {
        fun create() = Duck()
    }

    companion object    // A companion object can be used as the Kotlin equivalent to static methods in Java(=it’s shared by all instances of that class.) , which means you can use them without instantiating the class:

    // An object declaration inside a class defines another useful case: the companion object. Syntactically it's similar to the static methods in Java: you call object members using its class name as a qualifier. If you plan to use a companion object in Kotlin, consider using a package-level function instead.
    {
        fun create2() = Duck()
    }
}
object DuckManager  // this is a just like singleton class of a variable; here DuckManager variable has its own properties & functions ;
{                      // An object declaration defines a class and creates an instance of it in a single statement.
    val allDucks = mutableListOf<Duck> ()
    fun herdDucks()
    {}
}
fun main() {
    var selectedBandMember :BandMember = BandMember.JERRY   // assign one of BandMember's values    // selectedBandMember, can only be assigned a value for a valid band member.
    println(selectedBandMember.instrument)
    println(selectedBandMember.sings())

    val newDuck = Duck()
    val newDuck2 = Duck.DuckFactory.create()
    val newDuck3 = Duck.create2()

    val startingPoint = object      //    An object expression - an anonymous object on the fly // object expression: a simple object/properties structure. There is no need to do so in class declaration: you create a single object, declare its members and access it within one function. Objects like this are often created in Java as anonymous class instances.
    {
        val x = 0
        val y = 0
    }

    println(45.5.toDollar())
    println("test".halfLength())
    println("testP".halfLengthP)

    fun myFun()
    {
        listOf("A", "B", "C", "D").forEach {
            if (it == "C") return@forEach
            println(it)
        }
        println("Finsihed myFun()")

    }

    myFun()
   // ABDFinsihed myFun()

}

//receiver -> The subject of an extension function.(can be used in defining the logic as`this`)
//receiver type -> The type, an extension adds functionality to.
// Extensions let you add new members(=functions & properties) to an existing type without you having to create a whole new subtype. (even works for classes that are not marked open)
//Extensions are an alternative to the sharing behavior of inheritance ;   Since there is no open keyword on the String class definition, there is no way
//                                                                         to subclass String to add functionality through inheritance.
fun Double.toDollar() = "$$this"
fun String.halfLength() = this.length / 2.0
fun String.addExcl(amount :Int = 1) = this + "!".repeat(amount)
//public static final String addEnthusiasm(@NotNull String $receiver, int amount) {
//Intrinsics.checkParameterIsNotNull($receiver, "$receiver");
//return $receiver + StringsKt.repeat((CharSequence)"!", amount);
//}
infix fun String?.printWithDefault(default: String) = print(this ?: default)
val String.halfLengthP
    get() = length / 2.0
val String.numVowels
get() = this.count { it in "aeiou" }


data class Item(val name :String, val price :Float)
data class Order(val items :Collection<Item>)       // order can contain list/set of Item-objects
fun Order.maxPricedItemValue() :Float = items.maxBy { it.price }?.price ?: 0F
fun Order.maxPricedItemName() :String = items.maxBy {it.price}?.name ?: "NO_PRODUCTS"
val Order.commaDelimitedItemNames :String
    get() = items.map {it.name}.joinToString()  // extension property cannot be in a fun
fun extensionMembers()
{
    val order = Order(listOf(Item("Bread", 25.0F), Item("Wine", 29.0F), Item("Water", 12.0F)))
}
fun Any?.aFunction() :Any? {    // extension function on Any works with any type, but it does not maintains the type information
    println("$this a extension function on a Any") // this extension is not chainable, as it represents the object it returns as Any; & every Function can't be called on Any
    return this
}
fun <T> T.anExtensionFunctionForEveryType() :T {   //this extension is chainable, as it represents the object it returns as the same object; & the function that needs to use it can use
    println(this)       // generic extension function works with Any type, and it also maintains  the type information
    return this     // Now that the extension uses the generic type parameter T for the receiver and returns T instead of Any,
}                    // the particular type information for the receiver is passed forward in the chain of calls

//It is even possible to execute extensions on null references. In an extension function, you can check the object for null and use the result in your code:
fun <T> T?.nullSafeToString() = this?.toString() ?: "null"

fun extensionFunctionsForEveryType() {
    println(null.nullSafeToString())
    println("Kotlin".nullSafeToString())
    println(34.anExtensionFunctionForEveryType())
    //println(Person("hero").anExtensionFunctionForEveryType())
    println("any type".anExtensionFunctionForEveryType())
    println("".aFunction())
    println(null.aFunction())
    1 to 1.r()


}

fun Int.r(): RationalNumber = RationalNumber(this, 1)
fun Pair<Int, Int>.r(): RationalNumber = RationalNumber(first, second)

data class RationalNumber(val numerator: Int, val denominator: Int)









