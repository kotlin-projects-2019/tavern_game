class LootBox<Type :Loot>(vararg lootItems :Type) { // added a generic-type constraint to LootBox’s generic-type-parameter to  allow only descendants of the Loot class to be used with LootBox
    var open  = false                   // By using a type constraint, it is possible to constrain the contents to Loot and also preserve the specific subtype of the loot in the box.
        set(value){
            field = value
            println(if (value) "LootBox opened" else "LootBox closed")
        }
    private var loot = lootItems

    operator fun get(index :Int) = loot[index].takeIf {open}
    // both provides index based access
    fun fetch(item :Int) = loot[item].takeIf {open}           // loot will be returned if predicate in takeIf returns true
    fun <Type2> fetch(item :Int, lootModFunction :(Type) -> (Type2) )
            = lootModFunction(loot[item]).takeIf {open}    // Type2 will be returned if predicate in takeIf returns true


}
abstract class Loot(val value :Int)
class Fedora(val name :String, value :Int) :Loot(value)
class Coin(value :Int) :Loot(value)

fun main() {
    val lootBoxOne = LootBox(Fedora("a generic-looking fedora", 15),  Fedora("a dazzling magenta fedora", 25))
    val lootBoxTwo = LootBox(Coin(15))
    lootBoxOne.run {
        open = true
        fetch(0)?.run {
            println("You retrieve $name from the box!")     // If you used Loot for the type, that would constrain LootBox to accept descendants of Loot,
        }                                                   // but it would also discard the information that a Fedora was in the box.
        open = false                                        // , then to call Fedora specific code we would have to use the `is` operator
    }                                                       // as it would no longer be possible to See That The LootBox Contained Anything Other Than Loot.
    val coin = lootBoxOne.run{
        open = true
        fetch(0) {
            Coin(it.value * 3)  // can return anything from this version of fetch; whatever you return from the lambda, the fetch function will return that same type,
        }
    }
    coin?.run{
        println(value)
    }



}
