public open class RoomC(open val name :String) :Any() {
    protected open val dangerLevel = 5
    public open fun description() = "Room: $name\nDanger Level: $dangerLevel\nCreature: ${monster?.description ?: "none."}"
    public open fun load() :String = "Nothing much to see here..."
    public final var monster :Monster? = Goblin()
}

public open class TownSquareC :RoomC("Town Square") {   // as the class itself is final, so no need to add `final` to any `open` member
    /* protected in Parent -> public in Child */
    public override val dangerLevel = super.dangerLevel - 3
    private val bellSound = "GWONG"
     override fun load() = "The villagers rally & cheer as you enter!\n${ringBell()}"
     fun ringBell() = "The Bell Tower announces your arrival. $bellSound"
}

private fun main() {
    val currentRoom :RoomC = TownSquareC()
    currentRoom.run {
        p(description(), load()/*, ringBell()*/)
    }
    (currentRoom as TownSquareC).ringBell().run(::println)
    val abandonedTownSquareC = object : TownSquareC(){
        override fun load() = "You anticipate applause, but no one is here..."
    }

}

//When you override a function in Kotlin, the overriding function in the
//subclass is, by default,
//open to being overridden (as long as the subclass is marked open).


/*
-------------For class------------
public/private/internal open/final/abstract
------------For class-members---------------
public/private/protected/internal open/final override val/fun
*/

fun RoomC.configurePitGoblin(block: RoomC.(Goblin) -> Goblin): RoomC {
    val goblin = block(Goblin("Pit Goblin", description = "An Evil Pit Goblin"))
    monster = goblin
    return this
}
//configurePitGoblin extension to Room accepts a lambda that has Room as its receiver.
// The result is that the properties of Room are available within the lambda that you
//define, so the goblin can be configured using the Room receiver’s properties:

