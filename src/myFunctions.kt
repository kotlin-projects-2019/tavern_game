import java.lang.IllegalArgumentException

 fun p(vararg toPrint :Any?)    // Internal visibility marks a function, class, or property as public to other functions, classes, and properties within the same module.
{
    for (e in toPrint) println(e)
}
fun pr(vararg toPrint :Any?)
{
    for (e in toPrint) print(e)
}
//
//public final class MyFunctionsKt {
//    public static final void p(@NotNull Object... toPrint) {
//        Intrinsics.checkParameterIsNotNull(toPrint, "toPrint");
//        Object[] var3 = toPrint;
//        int var4 = toPrint.length;
//
//        for(int var2 = 0; var2 < var4; ++var2) {
//            Object e = var3[var2];
//            boolean var5 = false;
//            System.out.println(e);
//        }
//
//    }
//
//    public static final void pr(@NotNull Object... toPrint) {
//        Intrinsics.checkParameterIsNotNull(toPrint, "toPrint");
//        Object[] var3 = toPrint;
//        int var4 = toPrint.length;
//
//        for(int var2 = 0; var2 < var4; ++var2) {
//            Object e = var3[var2];
//            boolean var5 = false;
//            System.out.print(e);
//        }
//
//    }
fun strToInt(target :String) :Long {
    val tensPlace = { placeValue:Int ->
        var groupOfTens = 10L
        for (i in 1 until placeValue) groupOfTens *= 10L
        groupOfTens
    }
    val l = mutableListOf<Long>()   // 1. A MutableList to hold all the number-literals in the string
    for (e in target) {                       // 2. Separate out the literals in a each position of the list
        l += when (e) {
            '0' -> 0L
            '1' -> 1L
            '2' -> 2L
            '3' -> 3L
            '4' -> 4L
            '5' -> 5L
            '6' -> 6L
            '7' -> 7L
            '8' -> 8L
            '9' -> 9L
            else -> throw IllegalArgumentException("Target string contains letters")
        }
    }
    val noOfDigits = l.size                 // 3. Determine the number of digits
    l.reverse()                                 // 4. As we do the sum in reverse order , so reverse the digits
    var sum = l[0]
    for ( a in 1 until noOfDigits ) {       // 5. Sum of digits will be done 1 less time than the no. of digits are
        sum += l[a] * tensPlace(a)
    }
    return sum
}

