public final class Jhava extends Object {
    // here comes the class members
    //1. a normal function with `default` visibility, can be accessed only inside the same package

    String utterGreeting() {   // can be used by any code inside the same package as the calss
        return "BLARGH";
    }
    //2. krna the return String, kr diya null, wtf | In kotlin null cannot be a returned in place of a non-null type String
    String determineFreindshipLevel() { return null; }
}
//With some awareness of the
//differences in the two languages and the help of annotations available on each
//side, you can enjoy the best of what Kotlin has to offer.