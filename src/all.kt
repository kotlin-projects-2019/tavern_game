//import java.lang.Exception
//import java.lang.IllegalStateException
//
//fun main() = runSimulation()
//
//fun runSimulation()
//{
//    p(
//        configureGreetingFunction()("Guyal")
//    )
//}
//
//fun configureGreetingFunction() :(String) -> String     // Function Type as Return Type
//{
//    val structureType = "hospitals"
//    var numBuildings = 5
//    return { playerName ->
//        val currentYear = 2019
//        numBuildings++
//        println("Adding $numBuildings $structureType")
//        "Welcome to SimVillage, Mayor!  (copyright $currentYear)"
//
//    }
//
//}
//
//fun main2() = runSimulation("Guyal") { playerName, numBuildings  ->/*body/definition of the lambda -> Lambda-expression*/
//    val currentYear = 2019
//    println("Adding $numBuildings houses")
//    "Welcome to SimVillage, Mayor!  (copyright $currentYear)"   // this expression becomes unused as soon as the containing variables's type is changed to return type Unit
//}
//
//inline fun runSimulation(
//    playerName :String,
//    /*declaration/signature of the lambda -> Function-type)*/
//    greetingFunction :(String,Int) -> String ) {
//
//    val numBuildings = (1..3).shuffled().last()     // Randomly selects 1,2 or 3
//
//    /*call of the lambda*/
//    println(greetingFunction(playerName, numBuildings))
//}
//
//fun main3() {
//    var s :String? = readLine()
//    p(s, "s value")
//
//    readLine()?.let { if (it.isNotBlank()) it.capitalize()   }
//
//    readLine()!!.capitalize()
//
//    var beverage = readLine()
//    if (beverage != null) {
//        beverage.capitalize()       // smart cast to kotlin.String
//        beverage = (beverage as String).capitalize()
//    }
//    // you did not have to use the !!. operator when referencing
//    //beverage in beverage = beverage.capitalize() . The Kotlin compiler
//    //recognizes that beverage must be non-null as a condition for that branch, and
//    //it can deduce that a second null check is unnecessary. This feature – the
//    //compiler tracking conditions within an if expression – is an example of smart
//    //casting.
//}
//
//
//fun main4() {
//    var swordsJuggling :Int? = null
//    val isJugglingProficient = (1..3).shuffled().last() == 3    // 1 in 3 chance of being lucky
//    if (isJugglingProficient) swordsJuggling = 2
//
//    try     // what will happen till/if no exception occurs
//    { proficiencyCheck(swordsJuggling)
//        swordsJuggling = swordsJuggling!!.plus(1)
//    } catch (e :Exception)    // what will if exception occurs  // exception handled using a try/catch block, , so the rest of the code continues
//    {
//        p("there is an exception bro $e. Exactly what do you want to do HuH ?? ")
//        val a = readLine()
//        p("No can do : $a")
//    }
//    // One-third of the time, your sword-juggling proficiency enables you juggle a third sword. The other two-thirds of the time, your program crashes.
//    p("yOU jUGGLE $swordsJuggling sWORDS!")
//}
//
//fun proficiencyCheck(swordsJuggling :Int?)
//{
//    swordsJuggling ?: throw UnskilledSwordJugglerException("com.hfkotlin.mypackage.Player cannot juggle Swords") {   // 1st the code in this class's constructor is executed then its parent's constructor
//        "a lambda argument to a class's constructor"
//    }
//    // indicates that an illegal state has occurred and gives you the opportunity to add more information by passing a string to be printed when the exception is thrown.
//    // throwing an exception signals that the issue must be handled before execution continues.
//    checkNotNull(swordsJuggling) {
//        "its a lambda so can do what you did in that `init block`"
//    }
//}
//
//// To define a custom exception, you define a new class that inherits from some other exception.
//class UnskilledSwordJugglerException(msg :String = "", l :() -> Any = {}) : IllegalStateException("com.hfkotlin.mypackage.Player cannot juggle")// this msg would be with the exception; the value of e
//{
//    init{   // to be executed when the exception is thrown
//        p(msg)  // this msg would be on the console
//        l()
//    }
//}
//
//
//
//const val TAVERN_NAME = "Taernyl's Folly"
//fun main5() {
//    placeOrder("shandy,Dragon's Breath,5.91")
//
//    repeat(5) {
//        println("I am the hero")
//    }
//    for ( i in 0..4 step 2 ) {
//        println("In am the hero")
//    }
//
//    var firstItemSquared = listOf(1,2,3).first().let ({ it * it })
//    firstItemSquared = listOf(1,2,3).first().run ({ this * this })
//
//    var a = "Polarcubis, Supreme Master of NyetHack".let(::nameIsLong)
//    var b = "Polarcubis, Supreme Master of NyetHack".run(::nameIsLong)
//    var c = nameIsLong("Madrigal")
//
//    "Polarcubis, Supreme Master of NyetHack"
//        .run(::nameIsLong)
//        .run(::playerCreateMessage)
//        .run(::println)
//
//    "Polarcubis, Supreme Master of NyetHack"
//        .let(::nameIsLong)
//        .let(::playerCreateMessage)
//        .let(::println)
//
//    println(playerCreateMessage(nameIsLong("Polarcubis, Supreme Master of NyetHack")))
//
//
//}
////    |-----------THE LAMBDA BODY--------------|  // In T.let() `this` is passed as argument to the block
//fun nameIsLong(name: String) = name.length >= 20 // In T.run() , block body is called as on the reciever which leads in passing the reciever as argument `this`
//// ; just like T is a class & the block body is inside the T class
//fun playerCreateMessage(nameTooLong: Boolean): String {
//    return if (nameTooLong) {
//        "Name is too long. Please choose another name."
//    } else {
//        "Welcome, adventurer"
//    }
//}
//
//fun placeOrder(menuData :String)
//{
//    val indexOfApostrophe = TAVERN_NAME.indexOf("'")
//    val tavernMaster = TAVERN_NAME.substring(0 until indexOfApostrophe)
//
//    val (type, name, price) = menuData.split(",")   // List destructuring -> allows to declare & assign multiple variables in a single expression
//    val msg = "Madrigal buys a $name ($type) for $price."
//
//    val phrase = if (name == "Dragon's Breath") "Madrigal exclaims :${toDragonSpeak("Ah, delicious $name")}"
//    else "Madrigal says: Thanks for the $name."
//
//    // In many ways, a Kotlin String behaves like a list of characters as majority of the functions for traversing lists are also available for strings.
//
//    var a = "Dragon's Breath".forEach {
//        println("$it\n")
//    }   // forEach returns always Unit
//    var b = "Dragon's Breath".map {
//        it
//    }
//    var c = "Dragon's Breath".getOrElse(5) {
//        p("$it not available")
//        '?' }  // if index is out of bounds, the Result is of lambda
//
//    var i = price.toDoubleOrNull() ?: 0.0
//
//    val msg2 = "Price is %.2f for %d glass(es)".format(5.91, 1)    // Price is 5.91 for 1 glass(es)
//    // Kotlin’s format strings use the same style as the standard format strings in Java, C/C++, Ruby
//
//    43.let() {}
//
//}
//
//fun toDragonSpeak(phrase :String) :String
//{
//    val regex = Regex("[aeiou]")
//    // a regular expression to know the characters  then a lambda to what to act on them
//    return phrase.replace(regex) { a ->
//        when (a.value) {
//            "a","A" -> "4"
//            "e","E" -> "3"
//            "i","I" -> "1"
//            "o","O" -> "0"
//            "u","U" -> "|_|"
//            else -> a.value
//        }
//    }
//}
//fun a() {
////--------------------------------------------------------------------------------------------
//    val fileContents = File("myfile.txt").takeIf { it.canRead() && it.canWrite() }
//        ?.readText()
//
//    val file = File("myfile.txt")
//    val fileContents = if (file.canRead() && file.canWrite()) file.readText() else null
//
////--------------------------------------------------------------------------------------------
//    val uniquePatrons = mutableSetOf<String>()
//    (0..9).forEach { uniquePatrons += "${patronList.shuffled().first()} ${lastName.shuffled().first()}" }
//    val patronGold = mutableMapOf<String, Double>()
//    uniquePatrons.forEach {
//        patronGold[it] = 6.0
//    }
////--------------------------------------------------------------------------------------------
//
//    println(
//        mapOf(
//            "Eli" to 10.75,
//            "Mordoc" to 8.25,
//            "Sophie" to 5.50,
//            "Sophie" to 6.25
//        )
//    )
//// {Eli=10.5, Mordoc=8.0, Sophie=6.25}
//    val patronGold = mutableMapOf("Eli" to 5.0, "Sophie" to 1.0)
//    patronGold += "Sophie" to 6.0
//    println(patronGold)
////{Eli=5.0, Sophie=6.0}
//
//
//}
//
//interface RoomInterface {
//    val name :String   // property can't be initialized in a interface
//        get() = "still after a getter need an initializer in implementor"
//    //        set(v) {
////            "Can't use a backing-field in setter"
////        }
//    fun description() = "Room: $name"
//    fun load() :String
//}
//
//abstract class RoomC(nam :String) {
//    open val name
//        get() = ""
//    open fun description() = "Room: $name"
//    abstract fun load() :String
//}
//
//class SpecificRoomInterface(nam :String) :RoomInterface {
//    fun a() = name.capitalize()
//    //override var name = nam     // property initialization is important even if it has a getter in the <super>I as after overriding the inherited property you need to give an implementation for thatk
//    override fun load() = ""
//}
//
//class SpecificRoomC(nam :String) :RoomC(nam) {
//    override fun load() = ""
//}
//
//class TownSquare :RoomC("Town Square") {
//    override fun load(): String = "The villagers rally & cheer as you enter!"
//}