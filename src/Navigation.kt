data class Coordinate(val a :Int, val b :Int /*, a :Int*/) {
    val isInBounds = a >= 0 && b >= 0


    operator fun plus(other :Coordinate) = Coordinate(this.a + other.a , this.b + other.b)


//    fun component3() = isInBounds
}
// tie each Direction-type to the Coordinate change when the player moves in that direction
enum class Direction(val coordinate: Coordinate) {  // Because you add a parameter to constructor of enum, you will have to call that constructor when defining each enumerated type
    EAST(Coordinate(0,1))   // WorldMap[N/S][W/E]
    ,WEST(Coordinate(0,-1))
    ,SOUTH(Coordinate(1,0))
    ,NORTH(Coordinate(-1,0))
// functions are called on enumerated-types, not on enum-class itself
    ;fun updateCoordinate(playerCoordinate :Coordinate)    // changes the player locn based on their movement
            = this.coordinate + playerCoordinate

}
