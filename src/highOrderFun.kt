///*
//One way anonymous functions are used is to
//allow you to easily customize how built-in functions from the Kotlin standard
//library work to meet your particular needs. An anonymous function lets you
//describe additional rules for a standard library function so that you can
//customize its behavior. Anonymous functions let the standard library do what it does best – provide a
//foundation of functions and types for building great Kotlin applications –
//without including features that would be too specific to be considered
//“standard.”
//Anonymous functions implicitly, or automatically, return the last line of their function definition, allowing you to omit the return keyword.
//The return keyword is prohibited in an anonymous
//function because it could be ambiguous to the compiler which function the
//return is from: the function the anonymous function was invoked within, or the anonymous function itself.
//*/
//
//@file:Suppress("LocalVariableName")
//
//data class Grocery(val name :String, val category :String, val unit :String, val unitPrice :Double, val quantity :Int)
////    for ( r in groceries ) if (r.unitPrice > 5.0) println(r.name)
////    for ( r in groceries ) if (r.category == "Vegetable") println(r.name)
////    for ( r in groceries ) if (r.unit == "Pack") println(r.name)        // the same for loop 3 times , just a change in condn ; so wrap the condn as a lambda
//fun search (groceries :List<Grocery>, condn :(Grocery) -> Boolean)
//{
//    for (r in groceries) if (condn(r)) println(r.name)
//} // AN ANOTHER WAY TO DO THE SAME :-
//infix fun List<Grocery>.search1 (condn :(Grocery) -> Boolean)   // an extension fun with one parameter can be turned to infix-fun which gives alternate way to call the fun
//{
//    for (r in this) if (condn(r)) println(r.name)
//}
//fun main() {
//    val groceries = listOf(
//        Grocery("Tomatoes", "Vegetable", "lb", 3.0, 3),
//        Grocery("Mushrooms", "Vegetable", "lb", 4.0, 1),
//        Grocery("Bagels", "Bakery", "Pack", 1.5, 2),
//        Grocery("Olive Oil", "Pantry", "Bottle", 6.0, 1),
//        Grocery("Ice Cream", "Frozen", "Pack", 3.0, 2)
//    )
//
//    println("Expensive Ingredients :")
//    search(groceries) { obj: Grocery -> obj.unitPrice > 5.0 }  // we can refer to the ONLY SINGLE parameter in the lambda body using it, As the lambda-declaration has a single parameter of a known type
//    groceries.search1 { it.unitPrice > 5.0 }
//        // OR without dot operator
//    groceries search1 { it.unitPrice > 5.0 }
//
//    println("All Vegetables :")
//    search(groceries) { it.category == "Vegetable" } // All Vegetables :TomatoesMushrooms
//
//    val vegetables = groceries.filter { it.category == "Vegetable" }
////List of vegetables : [Grocery(name=Tomatoes, category=Vegetable, unit=lb, unitPrice=3.0, quantity=3), Grocery(name=Mushrooms, category=Vegetable, unit=lb, unitPrice=4.0, quantity=1)]
//
//    println("All packs :")
//    search(groceries) { it.unit == "Pack" }
//
//    val notFrozen = groceries.filterNot { it.category == "Frozen" }
//
//    println(groceries[2])
//
//    val `grocery in list groceries having highest unit price` = groceries.maxBy { obj: Grocery -> obj.unitPrice }   // the evaluation in lambda should return a number
//    val `grocery in list groceries having lowest unit price` = groceries.minBy { it.quantity }
//
//    val totalQuantity = groceries.sumBy { it.quantity }
//    val totalPrice = groceries.sumByDouble { it.quantity * it.unitPrice } // The lambda body tells the function what you want it to sum.
//
//
//    val ints = listOf(1, 2, 3, 4)
//    val minInt = ints.minBy { num: Int -> num }         // the evaluation in lambda should return a number
//    val sumInts2 = ints.sumBy { it }
//    var sumInts = ints.sum()
//
//    val sumInts3 = ints.sumByDouble { it.toDouble() }       // // the evaluation in lambda should return a double
//    val sumInts4 = ints.sumByDouble { int: Int -> int.toDouble() }  // same as above
//
//    // [1, 2, 3, 4] 1  2  3  4 1 10 10 10.0 10.0
//
//    val doubleInts = ints.map { print(it) }  // [kotlin.Unit, kotlin.Unit, kotlin.Unit, kotlin.Unit]
//    val groceryNames = groceries.map { it.name }    // List of each grocery name // [Tomatoes, Mushrooms, Bagels, Olive Oil, Ice Cream]
//
//
//    val newPrices =
//        groceries.filter { it.unitPrice > 3.0 }     // filter will return a List of references to grocery-objects
//            .map { it.unitPrice * 2 }          // map will return a list of their unitPrice x2-
//
//
//    var s = "h" + "i"
//    var itemNames = ""
//    for (obj in groceries) itemNames += obj.name    // just like a loop’s body has access to variables that have been defined outside the loop.
//    groceries.forEach { itemNames += it.name }              // lambdas have also access to variables
//
//
//    val groupByCategory: Map<String, List<Grocery>> =
//        groceries.groupBy { it.category }     // keys-type  = category-type   |   values-type = obj-passed
//
//
//    for ((category_keys, groceryList_values) in groupByCategory) {
//        println(category_keys)
//        for (obj in groceryList_values) println("    " + obj.name)
//    }
//
//    groupByCategory.forEach()       //  The forEach function traverses each element in the list – one by one, from left to right –
//    { (k, v) ->                               // and passes each element to the anonymous function you provide as an argument.
//        println(k)
//        v.forEach()
//        {
//            println("   " + it.name)
//        }
//    }
//
//    val sumOfInts = ints.fold(0) { initial_value, obj -> initial_value + obj }
//    //In the above example, the function takes the initial_value, adds
//    //it to the value of the current obj, and assigns this new value to runningSum.
//    //When the function has looped through all items in the collection, fold returns the final value of this variable.
//
//    println("\n" + sumOfInts)
//
//
//}
//
//
