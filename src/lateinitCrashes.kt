//class Player {
//    lateinit var alignment: String  // // initialized when assigned ; if referred before initialization , UninitializedPropertyAccessException
//    // could implement this pattern using nullable type, but then you would be required to handle your property's nullability throughout your codebase
//    fun determineFate() {
//        alignment = "Good"
//    }
//    fun proclaimFate() {
//        if (::alignment.isInitialized) println(alignment)
//    }
//
//    // here health is a property
//    val health = 100
//    init {                                                  // ensure that properties used in ANYWHERE are initialized
//        // here its local
//        val health = 100
//        val healthBonus = health.times(3)
//    }
//
//    // --------The below code compiles but running it will result in NullPointerException-----------------------
//    val name: String                            // as name-property is not initialized
//    private fun firstLetter() = name[0]
//    init {
//        println(firstLetter())                  // when accessed here
//        name = "Madrigal"                       // CODE COMPILES as name-property is initialized herE
//    } // compiler does not inspect the order properties are initialized in compared to the functions that use them WITHIN the init block.
//
//
//    // --------The below code compiles but running it will result in NullPointerException-----------------------
//    val playerName: String = initPlayerName()       // name2-property when accessed by the fun its not initialized
//    val name2 = ""
//    private fun initPlayerName() = name2
//
//
//}
//class Sword(_name: String) {
//    var name = _name        // at this time the value in property goes directly skipping the setter's logic
//        get() = "The Legendary $field"
//        set(value) {
//            field = value.toLowerCase().reversed().capitalize()
//        }
//
//    init {
//        name = _name        // at this time the value in property goes through setter, in the init block
//    }
//}
//
