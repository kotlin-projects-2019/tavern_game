fun main() {
    var la : (Int) -> String = {"print"}
     la = {"pri"
            "nt" }
    var x :(Int) -> Unit = {it + 5}
    var anyL :Any  = {x:Int -> x + 5}

    var lambda2 :(Int) -> Int = {(it * 3) - 4}
    var lambda4 :(Int) -> Double = {it + 7.1}

    var lambda3 :(Int) -> Unit = {x :Int -> x + 56}
    var lambd3 :(Int) -> Unit = {x :Int -> x + 56}

    var x2  = x as  (Int) -> Int


    // call the fun convert
    convert({ c :Double -> c * 1.8 + 32}, 20.0)
    convertFive()
    { it * 1.8 + 32 }
    convertFive { it * 1.8 + 32 }

    // call the fun getConversionLambda
    val pounds = getConversionLambda("KtoP") (2.5)
    // call the fun combine
    val kgsToPounds = { x: Double -> x * 2.204623 }
    val poundsToUSTons = { x: Double -> x / 2000.0 }
    val kgsToUSTons = combine(kgsToPounds, poundsToUSTons) // type of variable is inferred by the function return type
}

fun convert(converter :(Double) -> Double, x :Double) :Double
{
    val result = converter(x)       // invoke the lambda in the function body
    println("$x is converted to $result")
    return result
}

fun convertFive(converter: (Int) -> Double) : Double {
    val result = converter(5)
    println("5 is converted to $result")
    return result
}

fun getConversionLambda(str :String) :(Double) -> Double
{                                      //|\\
    when (str)                         //|\\
    {                                  //|\\
        "CtoF" -> return { i :Double -> i * 1.8 + 32 }             // { it * 1.8 + 32 }
        "KtoP" -> return { it * 2.204623 }                       // { i :Double -> i * 2.204623 }
        "PtoUST" -> return { it / 2000.0 }                      // { i :Double -> i / 2000.0 }
        else -> return { it }                                   // { i :Double -> i }
    }
}
typealias DD = (Double) -> Double
fun combine(lambda1 :DD, lambda2 :DD) :DD
= {lambda2(lambda1(it))}    // type of it is inferred by the function return type
// { noToc :Double -> lambda2(lambda1(noToc)) }
// called the code first for lambda1 with its parameter
// then the code of lambda2 with the return value of lambda1






