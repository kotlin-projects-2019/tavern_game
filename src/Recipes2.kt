//// concise code all over
//
//data class Recipe(val title :String, val mainIngredient :String, val isVegetarian :Boolean = false, val difficulty :String = "Easy")
//// class without body made possible due to primary constructor after class name
//
//data class Mushroom(val size :Int = 0, val isMagic :Boolean)
//{
//    constructor(isMagicP :Boolean = false) :this(0, isMagicP) // ALLOWS TO PASS DIFF PARAMETER COMBINATION TO CREATE OBJECTS
//}
//// no need of a secondary constructor in the class body as of default parameter values in primary constructor , as it also allows to create an object with different parameter combinations
//
//fun findRecipes(title :String = "", ingredient :String = "", isVegetarian :Boolean = false, difficulty :String = "" )
//= arrayOf(Recipe(title, ingredient, isVegetarian, difficulty))
//// functions without body due to single expression functions
//
//fun addNumbers(a :Int , b :Int = if (a == 4) 5 else a) = a + b
//fun addNumbers(a :Double, b :Double) = a + b
//
//fun main() {
//    val r1 = Recipe("Thai Curry", "Chicken")
//
//    val m = Mushroom(isMagic = false) // primary constructor
//    val m2 = Mushroom(false) // secondary constructor
//    println(m == m2)
//    val m1 = Mushroom(4, true)
//    val m3 = Mushroom()
//
//
//}
