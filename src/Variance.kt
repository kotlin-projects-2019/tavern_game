//class Barrel<out Type>(val item :Type)  // we insert a generic-Type-parameter to let the class accept any Type of parameter
//
//fun main() {
//    var fedoraBarrel: Barrel<Fedora> = Barrel(Fedora("a generic-looking fedora", 15))
//    var lootBarrel: Barrel<Loot> = Barrel(Coin(15))
//    lootBarrel = fedoraBarrel
//
//    val myFedora: Fedora = lootBarrel.item
//
//}
////
////fun <Type> randomOrBackupLoot(backupLoot : () -> Type) :Type {
////    val items = listOf(Coin(14), Fedora("a fedora of the ages", 150))
////    val randomLoot = items.shuffled().first()
////    return if (randomLoot is Type) randomLoot   // type-checking for a generic-parameter is not possible in usual way As generic-types are subject to type-erasure,
////            else backupLoot()                   //  meaning the type info for `Type` is not available at runtime, Java has the same rule
////
////}
//
//                                                //  but Kotlin provides the reified keyword, which allows to preserve the type info at runtime
//inline fun <reified Type> randomOrBackupLoot1(backupLoot : () -> Type) :Type {
//    val items = listOf(Coin(14), Fedora("a fedora of the ages", 150))
//    val randomLoot = items.shuffled().first()
//    return if (randomLoot is Type) randomLoot
//    else backupLoot()
//
//}
//
////type erasure -> The loss of type information for generics at runtime.
////reflection -> learning a name or a type of a property or function at runtime – generally a costly operation