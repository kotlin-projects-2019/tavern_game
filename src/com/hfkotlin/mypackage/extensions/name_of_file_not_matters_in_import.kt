package com.hfkotlin.mypackage.extensions   //-> WHERE IS THIS FILE IN THE src FOLDER
// to make use of any function in this `extensions` folder use import folder_from_src.function_name
fun <T> Iterable<T>.random() :T = this.shuffled().first()