package com.hfkotlin.mypackage
import com.hfkotlin.mypackage.extensions.random as randomizer
import p
import Coordinate
import java.io.File
import kotlin.math.pow
import Fightable
import variable

class Player(nam :String) :Fightable      // by default any fun/prop is public(==accessible from any file/fun in program)
{
    override val diceCount = variable
    override val diceSides = 6
//    override val damageRoll =
// player's attack function takes the result from damageRoll
    override fun attack(opponent: Fightable): Int {
        val damageDealt = if (isBlessed) {damageRoll * 2} else {damageRoll}
        opponent.healthPoints -= damageDealt
        return damageDealt
    }


    override var healthPoints: Int = 100
    var isBlessed: Boolean = false
        private set(value) {   // privateSetter-publicProperty == now property can be accessed from anywhere in program but can be modified only from within this class
            field = value
        }
    private var isImmortal: Boolean = false
    var square: Int = 2
        set(value) {
            field = value * value
            p("square of $value is $field")
        }
        get() {
            p("current value is at $field")
            return field
        }                            // properties -> val/var DECLARED AT CLASS LEVEL & IN PRIMARY CONSTRUCTOR anywhere else its a local-variable, even in INIT {}
    private var a = nam              // A backing field is always generated to store the value that property encapsulates(==variables declared with a var/val, means property of class);
    val name                         // nam is different thing here its an initializer, it has no backing field, & inside getters & setters only those variables can be accessed which
        get()                        // have a backing field as getters & setters use backing-field to read & write data that represents the property.
= "${a.capitalize()} of $homeTown"   // = "${nam.capitalize()} of $homeTown" -> NO CAN DO
    val karma = (Math.random().pow((110 - healthPoints) / 100.0) * 20).toInt()
    val aura = when (karma) {
        in 0..5 -> "red"
        in 6..10 -> "orange"
        in 11..15 -> "purple"
        in 16..20 -> "green"
        else -> "none"
    }
    var inebriationValue = 0
    val inebriationStatus                 // to protect property from assignment , now no need to make it private leave it public
        get() =
            when (inebriationValue) {
                in 1..10 -> "tipsy"
                in 11..20 -> "sloshed"
                in 21..30 -> "soused"
                in 31..40 -> "stewed"
                else -> "..t0aSt3d"
            }
    val condition
        get() = when (healthPoints) {
            100 -> "$name is in Excellent condition! ($healthPoints)"
            in 90..99 -> "$name is in Awful condition! ($healthPoints)"
            in 75..89 -> "$name has some minor wounds${if (isBlessed) " but is healing quickly!" else "."} ($healthPoints)"
            in 15..74 -> "$name looks pretty hurt. ($healthPoints)"
            else -> "$name is in awful condition. ($healthPoints)"
        } // conditions on the lefthand side of the branches evaluate to either true or false
    // , and others fall back to a default equality check

    fun castFireball(numFireballs: Int = 2): Int {
        if (numFireballs in 1..50) {
            p("A glass of Fireball springs into existence. (x$numFireballs)")
            this.inebriationValue = numFireballs
        } else p("Can't generate $numFireballs Fireball")

        return this.inebriationValue
    }

    val homeTown by lazy { File("data/towns.txt").readText()
        .split("\n").randomizer() }


    var currentCoordinate = Coordinate(0, 0)


}

