sealed class StudentStatus1{
    object NotEnrolled : StudentStatus1()
    class Active(val courseId: String) : StudentStatus1()
    object Graduated : StudentStatus1()
}


fun studentMessage1(status: StudentStatus1): String {
    return when (status) {
        is StudentStatus1.NotEnrolled -> "Please choose a course!"
        is StudentStatus1.Active -> "You are enrolled in: ${status.courseId}"
        is StudentStatus1.Graduated -> "Congratulations!"
    }}

class Student1(var status: StudentStatus1)

fun main1() {
    val student = Student1(StudentStatus1.Active("Kotlin101"))
    println(studentMessage1(student.status))
}

sealed class MessageType
class MessageSuccess(var msg :String) : MessageType()       // MessageSuccess & MessageFailure inherit from MessageType, and define their own properties in their constructors
class MessageFailure(var msg :String, var e :Exception) : MessageType()

fun main(args: Array<String>) {
    val messageSuccess = MessageSuccess("Yay!")
    val messageSuccess2 = MessageSuccess("It worked!")
    val messageFailure = MessageFailure("Boo!", Exception("Gone wrong."))

    var myMessageType :MessageType = messageFailure
    val myMessage = when (myMessageType) {
        is MessageSuccess -> myMessageType.msg
        is MessageFailure -> myMessageType.msg + " " + myMessageType.e.message
    }

    println(myMessage)

}

// sealed classes can have a limited number of subclasses