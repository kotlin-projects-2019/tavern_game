//fun main() {
////    val any :Array<Any> = arrayOf("string", 1, Wolf(), Hippo(), Vet(), Vehicle()) // to call a specific function need to check here each object-type
////    val roamables :Array<Roamable> = arrayOf(Wolf(), Hippo(), Vehicle()) // only to call the functions of Roamable without object-type check
////
////    for (e in any)  // iterate through collection ; val e = any[0], any[1],...
////    {
////       var x = when (e)
////        {
////            is String -> println(e.toUpperCase())
////            is Int -> println(e)
////            is Animal -> println("Animal is ${e.roam()}")
////            else -> println("Bhaggg!!! dkBose")
////        }
////        println("value of x - $x")
////    }
////
////    for (e in roamables) e.roam()
////
////    Wolf().velocity = 30
////    println(Wolf().velocity)
////    Hippo().velocity = 30
////    println(Hippo().velocity)
//
//    // println("${ == }")
//
//    var wolf = Wolf()
//    var hippo = Hippo()
//    println(hippo.equals(wolf))
//}
//
//
//class Vet
//{
//    fun giveShot(animal: Animal)
//    {
//        println("Vet shots to $animal")
//    }
//}
//
//interface Roamable  // allows me to create an array of roamable objects , based on a single common function in independent classes
//{
//
//    val objectIs : String
//    fun roam()
//    {
//        println("$objectIs Roams")
//
//    }
//    var velocity :Int
//    get() = 20
//    set(v) = println("implement the velocity function from Roamable; Can't update $v")
//
//
//}
//
// class Vehicle :Roamable
//{
//    override val objectIs = "Vehicle"
//
//}
//
//abstract class Animal :Roamable
//{
//    override fun equals(other: Any?): Boolean {
//        println("my equals implementation in Animal")
//        return super.equals(other)
//    }
//    abstract val image :String?
//    abstract val food :String?
//    abstract val habitat :String
//    var hunger = 10
//    abstract fun makeNoise()
//    abstract fun eat()
//    fun sleep()
//    {
//        println("Animal sleeps")
//    }
//}
//
//abstract class Canine :Animal()
//
//
//class Wolf :Canine()
//{
//    override val objectIs = "Canine"
//    override val image :String? = "Wolf.jpeg"
//    override val food = "Wolf food"
//    override val habitat = "Wolf habitat"
//    override fun makeNoise()
//    {
//        println("Howls!!!")
//    }
//
//    override fun eat() {
//        println("Wolf eats")
//    }
//}
//
//class Hippo :Animal()
//{
//    override val objectIs = "Hippo"
//    override var image = null
//    override val food = "Hippo food"
//    override val habitat = "Hippo habitat"
//    override fun makeNoise()
//    {
//        println("Grunts!!!")
//    }
//    override fun eat() {
//        println("Hippo eats")
//    }
//    fun sleep(prop : Int) // overloaded
//    {
//
//    }
//
//    override var velocity: Int = 2
//}